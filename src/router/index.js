import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ShowTodo from "@/views/ShowTodo";

const routes = [
    {
        path: '/todos',
        name: 'Home',
        component: Home
    },
    {
        path: '/todos/:todoId',
        name: 'showTodo',
        component: ShowTodo,
        props: true
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
